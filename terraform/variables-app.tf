variable "app_name" {
  type = string
  description = "application name"
}

variable "app_environment" {
  type = string
  description = "application environment"
}

variable "app_sources_cidr" {
  type = list(string)
  description = "List of IPv4 CIDR blocks from which to allow application access"
}

variable "admin_sources_cidr" {
  type = list(string)
  description = "List of IPv4 CIDR blocks from which to allow admin access"
}

