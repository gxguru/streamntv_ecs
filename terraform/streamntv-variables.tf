# streamntv container - streamntv-variables.tf
variable "streamntv_app_name" {
  description = "Name of Application Container"
  default = "streamntv"
}
variable "streamntv_app_image" {
  description = "Docker image to run in the ECS cluster"
  default = "206509048524.dkr.ecr.us-east-1.amazonaws.com/streamntv:latest"
}
variable "streamntv_app_port" {
  description = "Port exposed by the Docker image to redirect traffic to"
  default = 5000
}
variable "streamntv_app_count" {
  description = "Number of Docker containers to run"
  default = 2
}
variable "streamntv_fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default = "4096"
}
variable "streamntv_fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default = "8192"
}