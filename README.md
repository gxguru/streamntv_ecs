

In order to deploy terraform, we need to install terraform version >="0.12".

I use "0.12.26".

An IAM user with admin access should be created and the key and secret needs to be pushed to the `terraform.tfvars`
file.

Also in the region where it is deployed a pem file needs to be created. 
Steps to create is
1. Go to EC2 console in AWS
2. click on Key Pairs under Network & security
3. click create key pair with (PEM extension) for ssh access.